# SXW苏行网微信小程序

#### 介绍
苏行网小程序是一款旅行游记相关的产品，前端应用了uniapp、uview等多种技术，后台利用springboot、mybatis、爬虫等技术

#### 软件架构
后端技术：
	Springboot
	Mybatis

前端技术：
	uniapp
	uview


#### 使用说明
需要源码的请添加群聊


QQ群：


![输入图片说明](https://images.gitee.com/uploads/images/2020/1021/120344_4c46b069_1362984.png "苏行网粉丝交流群1群聊二维码.png")



微信群：


![输入图片说明](https://images.gitee.com/uploads/images/2020/1021/120422_c1477f23_1362984.jpeg "微信图片_20201021120359.jpg")



#### 小程序截图

首页


![输入图片说明](https://images.gitee.com/uploads/images/2020/1021/115957_e1e5c0a6_1362984.png "首页.png")



详情页1


![输入图片说明](https://images.gitee.com/uploads/images/2020/1021/120017_428705c2_1362984.png "详情页-1.png")



详情页2


![输入图片说明](https://images.gitee.com/uploads/images/2020/1021/120033_fa3e4718_1362984.png "详情页-2.png")



个人中心


![输入图片说明](https://images.gitee.com/uploads/images/2020/1021/120045_fcae7aee_1362984.png "个人中心.png")



收藏列表


![输入图片说明](https://images.gitee.com/uploads/images/2020/1021/120102_3c3f2653_1362984.png "收藏列表.png")



意见反馈


![输入图片说明](https://images.gitee.com/uploads/images/2020/1021/120154_f604cdbe_1362984.png "意见反馈.png")
